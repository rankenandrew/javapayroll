package edu.ranken.Andrew_Wolf;

import javax.swing.JOptionPane;
import java.text.DecimalFormat;




public class Payroll
{

    public static final int MAXNONOT = 40;
    public static final double OTRATE = 1.5;

    public static DecimalFormat df2 = new DecimalFormat(	"$###,#,###,##0.00");
    public static DecimalFormat df = new DecimalFormat("#,###,##0.00");

        public static void main(String[] args) {




            String firstName = "";
            String mi = "";
            String lastName = "";
            byte age = 0;
            boolean isUnion = false;
            int empNumber = 0;
            double hoursWorked = 0.0;
            double hourlyRate = 0.0;
            double grossPay = 0.0;
           // double overtimePay = 0.0;
            String inputStr = "";
            String outputStr = "";


            firstName = JOptionPane.showInputDialog("Please enter first name:");


            mi = JOptionPane.showInputDialog("Please enter middle initial:");


            lastName = JOptionPane.showInputDialog("Please enter last name:");


            inputStr = JOptionPane.showInputDialog("Please enter your age:");
            age = Byte.parseByte(inputStr);

            inputStr = JOptionPane.showInputDialog("Please enter union status(Enter true for Yes or false for No:");
            isUnion = Boolean.parseBoolean(inputStr);

            inputStr = JOptionPane.showInputDialog("Please input employee number:");
            empNumber = Integer.parseInt(inputStr);

            inputStr = JOptionPane.showInputDialog("Please input hours worked:");
            hoursWorked = Double.parseDouble(inputStr);

            inputStr = JOptionPane.showInputDialog("Please input hourly rate:");
            hourlyRate = Double.parseDouble(inputStr);



            if(hoursWorked <= MAXNONOT) {
                grossPay = hoursWorked * hourlyRate;
            }

            else {
                grossPay = (MAXNONOT * hourlyRate) + ((hoursWorked - MAXNONOT) * (hourlyRate * OTRATE));
            }

           // if(hourlyRate > MAXNONOT){


           // overtimePay = (hoursWorked - MAXNONOT) * (hourlyRate * OTRATE) + grossPay ;

          //  }
          //  else{

             //   grossPay = hoursWorked * hourlyRate;
          //      overtimePay = 0;
            //}


           // overtimePay = grossPay + overtimePay;



            outputStr += "First Name: " + firstName + "\n";
            outputStr += "Middle Init: " + mi + "\n";
            outputStr += "Last Name: " + lastName + "\n";
            outputStr += "Current Age: " + age + "\n";
            outputStr += "Union Status: " + isUnion + "\n";
            outputStr += "Employee #: " + empNumber + "\n";
            outputStr += "Hours Worked: " + (df.format(hoursWorked)) + "\n";
            outputStr += "Hourly Rate: " + (df2.format(hourlyRate)) + "\n";
            outputStr += "Gross Pay: " + (df2.format(grossPay)) + "\n";


            JOptionPane.showMessageDialog(null, outputStr);


            System.out.println("First Name: " + firstName);
            System.out.println("Middle Init: " + mi);
            System.out.println("Last Name: " + lastName);
            System.out.println("Current Age: " + age);
            System.out.println("Union Status: " + isUnion);
            System.out.println("Emp Number: " + empNumber);
            System.out.println("Hours Worked" + (df.format(hoursWorked)));
            System.out.println("Hourly Rate: " + (df2.format(hourlyRate)));
            System.out.println("Gross Pay: " + (df2.format(grossPay)));


        }

    }


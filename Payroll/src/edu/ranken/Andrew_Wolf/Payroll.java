package edu.ranken.Andrew_Wolf;

public class Payroll {

    public static void main(String[] args)
    {
        String firstName = "Jeffrey";	//employee first name
        char mi		= 'P'; //emp middle name
        String lastName = "Scott";	//emp last name
        byte	age	= 62;
        boolean isUnion	= false;
        int empNumber   = 12345;
        double hoursWorked = 40.0;
        double hourlyRate  = 25.0;
        double grossPay    = hoursWorked * hourlyRate; //emp gross pay


			/* Print everything out
				using System.out.println()
			*/
        System.out.println("First Name: " + firstName);
        System.out.println("Middle Init: " + mi );
        System.out.println("Last Name: " + lastName);
        System.out.println("Current Age: " + age);
        System.out.println("Union Status: " + isUnion);
        System.out.println("Emp Number: " + empNumber);
        System.out.println("Hours Worked" + hoursWorked);
        System.out.println("Hourly Rate: " + hourlyRate);
        System.out.println("Gross Pay: " + grossPay);


    }
}

package edu.ranken.Andrew_Wolf;

import java.util.Scanner;

public class Payroll {

    public static void main(String[] args) {
        String firstName = "";
        String mi = "";
        String lastName = "";
        byte age = 0;
        boolean isUnion = false;
        int empNumber = 0;
        double hoursWorked = 0.0;
        double hourlyRate = 0.0;
        double grossPay = 0.0;

        Scanner keyboard = new Scanner(System.in);

        System.out.print("Please input first name: ");
        firstName = keyboard.nextLine();

        System.out.print("Please input middle initial: ");
        mi = keyboard.nextLine();

        System.out.print("Please input last name: ");
        lastName = keyboard.nextLine();

        System.out.print("Please input current age: ");
        age = keyboard.nextByte();

        System.out.print("Please input union status:(Enter True for Yes or False for No) ");
        isUnion = keyboard.nextBoolean();

        System.out.print("Please input employee number: ");
        empNumber = keyboard.nextInt();

        System.out.print("Please input hours worked: ");
        hoursWorked = keyboard.nextDouble();

        System.out.print("Please input hourly rate: ");
        hourlyRate = keyboard.nextDouble();

        grossPay = hoursWorked * hourlyRate;


        System.out.println("First Name: " + firstName);
        System.out.println("Middle Init: " + mi);
        System.out.println("Last Name: " + lastName);
        System.out.println("Current Age: " + age);
        System.out.println("Union Status: " + isUnion);
        System.out.println("Emp Number: " + empNumber);
        System.out.println("Hours Worked" + hoursWorked);
        System.out.println("Hourly Rate: " + hourlyRate);
        System.out.println("Gross Pay: " + grossPay);

    }
}